# MGL7460: Réalisation & Maintenance de Logiciels (Projet Individuel )

## Informations Générales

  * Équipe projet :
    * Gerry O'Meara (Desjardins, [Site web Desjardins](https://Desjardins.com))
  * Sujet du travail: Analyse d'un projet du domaine public
  * choix du logiciel: [node-red](https://github.com/node-red/node-red)
  
### Objectif du travail

Analyse de la maintenabilité d'un logiciel du domaine public

** [Source code](https://github.com/node-red/node-red)

## Travail effectué pour livraison 1:  [RAPPORT PDF](https://gitlab.com/necplusultra/mgl7460-individuel/-/blob/master/RAPPORTS/rapport_intermediaire.pdf)


## Utilisation des scripts d'extraction git
### préalablement, le projet GIT doit etre cloné dans le répertoire suivant:

        ../../[nom du projet git]

### pour extraire des données brute:

        - à partir du répertoire Scripts:
        - exécuter la commande bash pour créé les données brute:
            ./CreationRawData.sh [nom du projet git]
            
        - les données brute se retouve dans le fichier:
            rawData_[nom du projet git].csv
        
        
            
### pour analyser les données brute:

        - à partir du répertoire Scripts:
        - exécuter la commande bash pour créé les données brute:
            DATA/AnalyseRawData.sh [nom du projet git]
        - les données analysées se retouveent dans les fichiers:
            DATA/AnalyseLastContrib[nom du projet git].csv
            DATA/AnalyseModdedFiles[nom du projet git].csv
            DATA/AnalyseModdedFilesPerUserPerCommit[nom du projet git].csv
            DATA/AnalyseNumCommits[nom du projet git].csv
            
            
        
        

 

