#!/usr/bin/env bash

#
# ouput une ligne csv par commit, contributeur, date du commit, fichier
#
FilesFromHash() {
  git show --pretty="" --name-status $1
}

usersFromGit() {
#  git log --pretty=format:"%h,%an,%ad" --date=short
  git log --pretty=format:$1 --date=short
}


GITPROJECT=$1
SCRIPTDIR=$(pwd)
OUTPUTFILE=$SCRIPTDIR/../DATA/rawData_$GITPROJECT.csv

echo analysing $GITPROJECT
echo outputting raw data to $OUTPUTFILE


cd ../../$GITPROJECT


#
# obtenir la liste des commit avec les contributeuirs (author)
#
IFS=$'\r\n' GLOBIGNORE='*' command eval  'contrbuteurs=($(usersFromGit "%h,%an,%ad,%ae" ))'
#IFS=$'\r\n' GLOBIGNORE='*' command eval  'contrbuteurs=($(git log --pretty=format:"%h,%an,%ad" --date=short))'
echo "${XYZ[5]}"
for contrbuteur in "${contrbuteurs[@]}"
do
  #
  # obtenir le hash de chaque commit p[ar contributeur
  #
  a=$(echo $contrbuteur| awk -F ',' '{print $1 }' )

  #
  #  obtenir la liste des fichiers pour chaque commit
  #
  IFS=$'\r\n' GLOBIGNORE='*' command eval  'filesincommit=($(FilesFromHash $a))'
  for f in "${filesincommit[@]}"
  do
  #  echo $f
    fileDetail=$(echo ${f// /,})
    fileDetail=$(echo ${fileDetail// /,})
    echo "$contrbuteur,$fileDetail" >>$OUTPUTFILE
   # echo "$contrbuteur,$fileDetail"

  done

done

echo analyse du raw data
cd $SCRIPTDIR
./AnalyseRawData.sh $GITPROJECT

