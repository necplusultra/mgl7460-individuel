#!/usr/bin/env bash

GITPROJECT=$1
INPUTFILE=../DATA/rawData_$GITPROJECT.csv
OUTPUTFILE=../DATA/$GITPROJECT.csv

OUTPUTanalyseLastContrib=../DATA/AnalyseLastContrib$GITPROJECT.csv
OUTPUTAnalyseModdedFiles=../DATA/AnalyseModdedFiles$GITPROJECT.csv
OUTPUTAnalyseModdedFilesPerUserPerCommit=../DATA/AnalyseModdedFilesPerUserPerCommit$GITPROJECT.csv
OUTPUTAnalyseNumCommits=../DATA/AnalyseNumCommits$GITPROJECT.csv
OUTPUTFinal=../DATA/AnalyseFinale$GITPROJECT.csv
OUTPUTcourriel=../DATA/contribCourriel$GITPROJECT.csv


#echo -------------------------------------------------------
#echo analyse du fichier raw data $INPUTFILE
#echo -------------------------------------------------------

#  last contribution
declare -A analyseLastContrib
declare -A keys
#echo Analyse dernier contribution  $OUTPUTanalyseLastContrib
awk -F, '{if (a[$2] < $3)a[$2]=$3;}END{for(i in a){print i","a[i];}}' OFS=, $INPUTFILE >> $OUTPUTanalyseLastContrib
while IFS="," read f1 f2; do          analyseLastContrib[$f1]=$f2;  done < $OUTPUTanalyseLastContrib
while IFS="," read f1 f2; do          keys[$f1]=$f1;  done < $OUTPUTanalyseLastContrib


# number of files modified
declare -A AnalyseModdedFiles
#echo Analyse nombre fichier par commit  $OUTPUTAnalyseModdedFiles
awk -F, '{a[$2]++;}END{for (i in a)print i","a[i];}' $INPUTFILE|sort >>  $OUTPUTAnalyseModdedFiles
while IFS="," read f1 f2; do            AnalyseModdedFiles[$f1]=$f2;  done <  $OUTPUTAnalyseModdedFiles

#  files per commit par usager
declare -A AnalyseModdedFilesPerUserPerCommit
#echo Analyse fichier per commit par contributeur $OUTPUTAnalyseModdedFilesPerUserPerCommit
awk -F, '{a[$2","$3]++; }END{for (i in a)print i","a[i];}' $INPUTFILE|sort >> $OUTPUTAnalyseModdedFilesPerUserPerCommit
while IFS="," read f1 f2; do            AnalyseModdedFilesPerUserPerCommit[$f1]=$f2;  done <  $OUTPUTAnalyseModdedFilesPerUserPerCommit

# number of commits
declare -A AnalyseNumCommits
#echo Analyse nombre commit par contributeur $OUTPUTAnalyseNumCommits
awk -F',' '{print $1","$2}' $INPUTFILE|uniq|awk -F, '{a[$2]++;}END{for (i in a)print i","a[i];}' >> $OUTPUTAnalyseNumCommits
while IFS="," read f1 f2; do            AnalyseNumCommits[$f1]=$f2;  done <  $OUTPUTAnalyseNumCommits


# courriel
declare -A courrielContrib
#echo courriel des contributeurs $OUTPUTAnalyseNumCommits
awk -F',' '{print $2","$4}' $INPUTFILE|uniq >> $OUTPUTcourriel
while IFS="," read f1 f2; do            courrielContrib[$f1]=$f2;  done < $OUTPUTcourriel



echo contributeur,LastContrib,NumCommits,ModdedFiles,Courriel
for k in "${keys[@]}"
do
    echo $k,${analyseLastContrib[$k]},${AnalyseNumCommits[$k]},${AnalyseModdedFiles[$k]},${courrielContrib[$k]}
done

#declare -A b
#while IFS="," read f1 f2 f3; do           echo $f1 $f2;b[$f1]=$f2;  done < t.csv
#echo ${b[Hugobox]}